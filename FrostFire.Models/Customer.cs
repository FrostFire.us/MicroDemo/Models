﻿using System.ComponentModel.DataAnnotations;

namespace FrostFire.Models
{
    public class Customer
    {
        public string GivenName { get; set; }
        public string FamilyName { get; set; }
    }
}