﻿using System;
using System.Collections.Generic;

namespace FrostFire.Models
{
    public class Order
    {
        public string OrderID { get; set; }
        public DateTime OrderDateTime { get; set; }
        public Customer Customer { get; set; }
        public IEnumerable<OrderItem> Items { get; set; }
    }
}