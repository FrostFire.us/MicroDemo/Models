﻿namespace FrostFire.Models
{
    public class OrderItem
    {
        public string RetailerCountry { get; set; }
        public string RetailerType { get; set; }
        public string OrderMethod { get; set; }
        public int Quantity { get; set; }
        public string ProductLine { get; set; }
        public string ProductType { get; set; }
        public string ProductName { get; set; }
        public double Price { get; set; }
    }
}